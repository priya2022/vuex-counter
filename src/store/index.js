import { createStore } from 'vuex'
import axios from 'axios'
// This is vuex store where state/ methods /computed /properties are centralized here
export default createStore({
  state: {
    count:0,
    colorCode:'red'
  },
 
  mutations: {
    // For accessing state and update/ changes occur
    decrease(state, randomNumber){
      state.count -= randomNumber
    },
    increase(state, randomNumber){
      console.log("randomNumber", randomNumber)
      state.count += randomNumber
    },
    setColorCode(state, newValue){
      state.colorCode = newValue
    }
  },
  actions: {
    // async funciton where api is called {i used random number generates}
    increase({commit}){
      axios(`https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new`)
      .then(response => {
       commit('increase', response.data)
      })
    },
    decrease({commit}){
      axios(`https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new`)
      .then(response => {
        commit('decrease', response.data)
      })
    },
    setColorCode({commit}, newValue){
      commit('setColorCode', newValue)
    }
  },
  getters: {
    // computed {filter properties applies here}
    counterSquare(state){
      return state.count * state.count
    }
  },
  modules: {
  }
})
